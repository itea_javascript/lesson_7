/*

  Composition vs Inheritance
  Компоновка (Композиция) против Наследования.

  Наследование, определяет обьект по тому чем он ЯВЛЯЕТСЯ.
  Композиция, определяет обьект по тому, что он ДЕЛАЕТ

  Опишем проблематику. Слайды!

*/

// Our Functions

const Drive = ( state ) => ({
  drive: () => console.log('Wroooom!, It\'s a car ' + state.name )
});

const Refill = ( state ) => ({
  refill: () => console.log( state.name + ' was refiled')
});

const Move = ( state ) => ({
  move: () => console.log( state.name + ' is moving. Speed ->' + state.speed )
});

const Fly = ( state ) => ({
  fly: () => console.log( state.name + ' flying into sky! Weather is ' + state.weather )
});

// Проверим ф-ю
Refill({name: "Volkswagen"}).refill(); // Volkswagen was refiled

// Наш конструктор.
const EcoRefillDrone = (name) => {
  let state = {
    name,
    speed: 100,
    weather: 'rainy'
  };

  return Object.assign(
    {},
    Drive(state),
    Refill(state),
    Fly(state)
  );
};

  const myDrone = EcoRefillDrone('JS-Magic');
  console.log( myDrone );
        myDrone.drive();
        myDrone.refill();
        myDrone.fly();


  /*

    Задание:
      Написать класс на композиции.
      Тематика - птицы.

      Птицы могут:
        - Нестись
        - Летать
        - Плавать
        - Кушать
        - Охотиться
        - Петь
        - Переносить почту
        - Бегать

    Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

   */
